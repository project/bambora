<?php

namespace Drupal\bambora;

use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Beanstream\Gateway;

/**
 * A utility service providing functionality related to Commerce Bambora.
 */
class ApiService {

  /**
   * Returns an initialized Beanstream Payments API client.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $gateway
   *   The payment gateway we're using.
   * @param string $api
   *   Api type.
   */
  public function payments(PaymentGatewayInterface $gateway, $api = 'www') {
    return $this->gateway($gateway, 'payments', $api)->payments();
  }

  /**
   * Returns an initialized Beanstream Profiles API client.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $gateway
   *   The payment gateway we're using.
   */
  public function profiles(PaymentGatewayInterface $gateway) {
    return $this->gateway($gateway, 'profiles')->profiles();
  }

  /**
   * Returns an initialized Beanstream Gateway client.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway
   *   The payment_gateway we're using.
   * @param string $type
   *   If we're initializing Beanstream for a profile/payment request.
   * @param string $apiMode
   *   Api type.
   *
   * @return \Beanstream\Gateway
   *   The Beanstream Gateway class.
   */
  public function gateway(
    PaymentGatewayInterface $payment_gateway,
    $type = 'profiles',
    $apiMode = 'www'
  ) {
    $config = $payment_gateway->getPlugin()->getConfiguration();

    $api_key = $type === 'profiles'
      ? $config['profiles_api_key']
      : $config['payments_api_key'];

    // Create Beanstream Gateway.
    $beanstream = new Gateway(
      $config['merchant_id'],
      $api_key,
      $apiMode,
      'v1'
    );

    return $beanstream;
  }

}
