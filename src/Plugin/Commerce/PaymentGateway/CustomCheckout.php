<?php

namespace Drupal\bambora\Plugin\Commerce\PaymentGateway;

use Drupal\Core\Url;
use Drupal\Core\Link;
use Beanstream\Exception;
use Drupal\commerce_price\Price;
use Drupal\bambora\ApiService;
use Beanstream\Profiles as ProfilesApi;
use Drupal\commerce_payment\CreditCard;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_price\RounderInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\commerce_payment\PaymentTypeManager;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;

/**
 * Provides the Bambora Custom Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "bambora_custom_checkout",
 *   label = "Bambora (Custom Checkout)",
 *   display_label = "Bambora",
 *   forms = {
 *     "add-payment-method" = "Drupal\bambora\PluginForm\Bambora\PaymentMethodAddForm",
 *     "edit-payment-method" = "Drupal\bambora\PluginForm\Bambora\PaymentMethodEditForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "diners", "jcb", "mastercard", "discover", "visa",
 *   },
 * )
 */
class CustomCheckout extends OnsitePaymentGatewayBase implements CustomCheckoutInterface {

  /**
   * Holds the period in seconds after which an authorization expires (29 days).
   */
  const AUTHORIZATION_EXPIRATION_PERIOD = 2505600;

  /**
   * The rounder.
   *
   * @var \Drupal\commerce_price\RounderInterface
   */
  protected $rounder;

  /**
   * The Bambora API helper service.
   *
   * @var \Drupal\bambora\ApiService
   */
  protected $apiService;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager $payment_type_manager,
    PaymentMethodTypeManager $payment_method_type_manager,
    TimeInterface $time,
    RounderInterface $rounder,
    ApiService $api_service,
    MessengerInterface $messenger
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_type_manager,
      $payment_type_manager,
      $payment_method_type_manager,
      $time
    );

    $this->rounder = $rounder;
    $this->apiService = $api_service;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('commerce_price.rounder'),
      $container->get('bambora.api_service'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'merchant_id' => '',
      'payments_api_key' => '',
      'profiles_api_key' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#default_value' => $this->configuration['merchant_id'],
      '#required' => TRUE,
    ];

    $form['payments_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Payments API Key'),
      '#description' => $this->t('This is the API Passcode found under
        Administration -> Account Settings -> Order Settings -> Payment Gateway
        -> Security/Authentication.'),
      '#default_value' => $this->configuration['payments_api_key'],
      '#required' => TRUE,
    ];

    $form['profiles_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Profiles API Key'),
      '#description' => $this->t('This is the API Passcode found under
        Configuration -> Payment Profile Configuration -> Security Settings.'),
      '#default_value' => $this->configuration['profiles_api_key'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

      $this->configuration['merchant_id'] = $values['merchant_id'];
      $this->configuration['payments_api_key'] = $values['payments_api_key'];
      $this->configuration['profiles_api_key'] = $values['profiles_api_key'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $payment_method = $payment->getPaymentMethod();

    if (empty($payment_method)) {
      // Get order.
      $order = $payment->getOrder();
      // Clear data.
      $order->setData('cc_data', NULL);
      $this->messenger->addError('Could not charge the payment method. Message: Payment method is not recognized.');
      throw new HardDeclineException('Could not charge the payment method. Message: Payment method is not recognized.');
    }

    // Check billing is empty.
    if (empty($payment_method->getBillingProfile()->get('address')->first()->getAddressLine1())) {
      $this->messenger->addError('No address in billing information.');
      throw new HardDeclineException('No address in billing information.');
    }

    $this->assertPaymentState($payment, ['new']);
    $this->assertPaymentMethod($payment_method);

    $payments_api = $this->apiService->payments($payment->getPaymentGateway());

    // If the user is authenticated.
    $owner = $payment_method->getOwner();
    if ($owner && $owner->isAuthenticated()) {
      $customer_id = $this->getRemoteCustomerId($owner);
      $remote_id = $payment_method->getRemoteId();
    }
    else {
      // If user is anonymous.
      $customer_id = $payment_method->getRemoteId();
      // Retrieve the customer's cards and use the last added card to charge.
      $cards = $this->apiService->profiles($payment->getPaymentGateway())
        ->getCards($customer_id);

      $card = end($cards['card']);
      $remote_id = $card['card_id'];
    }
    try {
      // Get order items.
      $order = $payment->getOrder();
      // Example of comments: "Eligibility Fee, Jacket(2x))".
      $orderComments = '';
      foreach ($order->getItems() as $product) {
        $orderComments .= ($orderComments ? ", " : "") . ucfirst($product->getTitle()) . (($product->getQuantity() > 1) ? ("(" . $product->getQuantity() . "x)") : "");
      }
      // Authorize the payment.
      $profile_payment_data = [
        'order_number' => $payment->getOrderId(),
        'amount' => $this->rounder->round($payment->getAmount())->getNumber(),
        'comments' => $orderComments,
      ];
      if ($payment_method->isReusable()) {
        $result = $payments_api
          ->makeProfilePayment(
          $customer_id,
          $remote_id,
          $profile_payment_data,
          $capture
        );
      }
      else {
        $ccData = $order->getData('cc_data', []);
        $singularData = array_merge($profile_payment_data, $ccData);
        $result = $payments_api
          ->makeCardPayment(
          $singularData,
          $capture
        );
      }
      // Clear data.
      $order->setData('cc_data', NULL);
    }
    catch (Exception $e) {
      // Get order.
      $order = $payment->getOrder();
      // Clear data.
      $order->setData('cc_data', NULL);
      $this->messenger->addError('Could not charge the payment method. Message: ' . $e->getMessage());
      throw new HardDeclineException('Could not charge the payment method. Message: ' . $e->getMessage());
    }

    $next_state = $capture ? 'completed' : 'authorization';
    if (!$capture) {
      $payment->setExpiresTime($this->time->getRequestTime() + self::AUTHORIZATION_EXPIRATION_PERIOD);
    }
    $payment->setState($next_state);
    $payment->setRemoteId($result['id']);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);
    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $amount = $this->rounder->round($amount);

    // Capture the payment.
    try {
      $this->apiService->payments($payment->getPaymentGateway())
        ->complete(
          $payment->getRemoteId(),
          $amount->getNumber()
      );
    }
    catch (Exception $e) {
      // Get order.
      $order = $payment->getOrder();
      // Clear data.
      $order->setData('cc_data', NULL);
      throw new PaymentGatewayException('Could not capture the payment. Message: ' . $e->getMessage());
    }

    $payment->setState('completed');
    $payment->setAmount($amount);
    $payment->save();
  }

  /**
   * Asserts that the payment method is neither empty nor expired.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *   The payment method.
   *
   * @throws \InvalidArgumentException
   *   Thrown when the payment method is empty.
   * @throws \Drupal\commerce_payment\Exception\HardDeclineException
   *   Thrown when the payment method has expired.
   */
  protected function assertPaymentMethod(PaymentMethodInterface $payment_method = NULL) {
    if (empty($payment_method)) {
      throw new \InvalidArgumentException('The provided payment has no payment method referenced.');
    }
    if ($payment_method->isExpired()) {
      throw new HardDeclineException('The provided payment method has expired');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);

    // Void the payment.
    try {
      $this->apiService->payments($payment->getPaymentGateway())
        ->voidPayment(
          $payment->getRemoteId(),
          $this->rounder->round($payment->getAmount())->getNumber()
        );
    }
    catch (Exception $e) {
      throw new PaymentGatewayException('Could not void the payment. Message: ' . $e->getMessage());
    }

    $payment->setState('authorization_voided');
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(
    PaymentInterface $payment,
    Price $amount = NULL
  ) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $amount = $this->rounder->round($amount);
    $this->assertRefundAmount($payment, $amount);

    // Refund the payment.
    try {
      $this->apiService->payments($payment->getPaymentGateway())
        ->returnPayment(
          $payment->getRemoteId(),
          $amount->getNumber(),
          $payment->getOrderId()
        );
    }
    catch (Exception $e) {
      throw new InvalidRequestException('Could not refund the payment. Message: ' . $e->getMessage());
    }

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(
    PaymentMethodInterface $payment_method,
    array $payment_details
  ) {

    // Create the actual payment method depending on new/existing customer.
    $remote_payment_method = $this->doCreatePaymentMethod(
      $payment_method,
      $payment_details
    );
    $card = end($remote_payment_method['card']);
    $payment_method->card_type = $this->mapCreditCardType($card['card_type']);
    $payment_method->card_number = substr($card['number'], -4);
    $payment_method->card_exp_month = $card['expiry_month'];

    $fourDigitYear = \DateTime::createFromFormat('y', $card['expiry_year']);
    $fourDigitYear = $fourDigitYear->format('Y');
    $payment_method->card_exp_year = $fourDigitYear;
    // Expiration time.
    $expires = CreditCard::calculateExpirationTimestamp(
      $card['expiry_month'],
      $fourDigitYear,
    );

    // Set the remote ID.
    $owner = $payment_method->getOwner();
    if ($owner && $owner->isAuthenticated()) {
      $remote_id = $card['card_id'];
    }
    else {
      $remote_id = $remote_payment_method['customer_code'];
    }
    $payment_method->setRemoteId($remote_id);
    $payment_method->setExpiresTime($expires);
    $payment_method->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    $owner = $payment_method->getOwner();
    // If there's no owner we won't be able to delete the remote payment method
    // as we won't have a remote profile. Just delete the payment method locally
    // in that case.
    if (!$owner || !$payment_method->isReusable()) {
      $payment_method->delete();
      return;
    }

    // Delete the remote record on Bambora.
    try {
      $this->apiService->profiles($payment_method->getPaymentGateway())
        ->deleteCard(
          $this->getRemoteCustomerId($owner),
          $payment_method->getRemoteId()
        );
    }
    catch (Exception $e) {
      // Skip the throw if Card is not found on payment vendor.
      if ($e->getCode() != 44) {
        throw new InvalidRequestException('Could not delete the payment method. Message: ' . $e->getMessage());
      }
    }

    $payment_method->delete();
  }

  /**
   * Updates the given payment method.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *   The payment method.
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   *   Thrown when the transaction fails for any reason.
   */
  public function updatePaymentMethod(PaymentMethodInterface $payment_method) {
    $owner = $payment_method->getOwner();
    // If there's no owner we won't be able to update the remote payment method
    // as we won't have a remote profile. Just delete the payment method locally
    // in that case.
    if (!$owner) {
      $payment_method->delete();
      return;
    }

    try {
      // Prepare card data to update.
      $cardId = $payment_method->getRemoteId();
      $data = [
        'card' => [
          'expiry_month' => $payment_method->get('card_exp_month')->getValue()[0]['value'],
          'expiry_year' => $payment_method->get('card_exp_year')->getValue()[0]['value'] % 100,
        ],
      ];
      $this->apiService->profiles($payment_method->getPaymentGateway())
        ->updateCard(
          $this->getRemoteCustomerId($owner),
          $cardId,
          $data,
        );
    }
    catch (Exception $e) {
      // Skip the throw if Card is not found on payment vendor.
      throw new InvalidRequestException('Could not update the payment method. Message: ' . $e->getMessage());
    }

  }

  /**
   * Creates the payment method on the Bambora gateway.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *   The payment method.
   * @param array $payment_details
   *   The gateway-specific payment details.
   *
   * @return array
   *   The details of the entered credit card.
   */
  protected function doCreatePaymentMethod(
    PaymentMethodInterface $payment_method,
    array $payment_details
  ) {
    $profiles_api = $this->apiService->profiles($payment_method->getPaymentGateway());
    $result = [];
    $customer_id = NULL;

    $owner = $payment_method->getOwner();

    if ($owner && $owner->isAuthenticated()) {
      $customer_id = $this->getRemoteCustomerId($owner);
    }

    if (!$payment_method->isReusable()) {
      $billing_profile = $payment_method->getBillingProfile();
      $address = $billing_profile->get('address')->first();
      $phone = $billing_profile->get('phone');
      $fourDigitYear = \DateTime::createFromFormat('Y', $payment_details['payment_wrap']['expiration']['expiry_year']);
      $fourDigitYear = $fourDigitYear->format('y');
      $card_data = [
        'card' => [
          0 => [
            'name' => @$payment_details['payment_wrap']['name'] ?: $address->getGivenName() . ' ' . $address->getFamilyName(),
            'number' => $payment_details['payment_wrap']['number'],
            'expiry_month' => $payment_details['payment_wrap']['expiration']['expiry_month'],
            'expiry_year' => $fourDigitYear,
            'cvd' => $payment_details['payment_wrap']['cvd'],
            'card_id' => uniqid('incognito_'),
            'card_type' => $this->getCardType($payment_details['payment_wrap']['number']),
          ],
        ],
        'billing' => [
          'name' => $address->getGivenName() . ' ' . $address->getFamilyName(),
          'email_address' => !empty($owner->getEmail()) ? $owner->getEmail() : $payment_details['bambora_customer_email'],
          'phone_number' => $phone->getString(),
          'address_line1' => $address->getAddressLine1(),
          'address_line2' => $address->getAddressLine2(),
          'city' => $address->getLocality(),
          'province' => $address->getAdministrativeArea(),
          'postal_code' => $address->getPostalCode(),
          'country' => $address->getCountryCode(),
        ],
      ];

      return $card_data;
    }
    // If the customer id already exists, use the Bambora token to retrieve
    // the customer.
    if (isset($customer_id)) {
      $result = $this->doCreatePaymentMethodForExistingCustomer(
        $payment_method,
        $payment_details,
        $customer_id,
        $profiles_api
      );
    }
    // If this is a new customer create both the customer and the payment
    // method.
    else {
      $result = $this->doCreatePaymentMethodForNewCustomer(
        $payment_method,
        $payment_details,
        $profiles_api
      );
    }

    return $result;
  }

  /**
   * Creates the payment method for an existing customer.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *   The payment method.
   * @param array $payment_details
   *   The gateway-specific payment details.
   * @param int $customer_id
   *   The profile ID of the customer from Bambora.
   * @param \Beanstream\ProfilesApi $profiles_api
   *   The Beanstream Profiles API client.
   *
   * @return array
   *   The details of the last entered credit card.
   */
  protected function doCreatePaymentMethodForExistingCustomer(
    PaymentMethodInterface $payment_method,
    array $payment_details,
    $customer_id,
    ProfilesApi $profiles_api
  ) {
    /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $address */
    $address = $payment_method->getBillingProfile()->get('address')->first();
    $user = $payment_method->getOwner();

    try {
      // Convert year to short format.
      $expiryYear = \DateTime::createFromFormat('Y', $payment_details['payment_wrap']['expiration']['expiry_year']);
      $expiryYear = $expiryYear->format('y');
      // Create a payment method for an existing customer.
      $card_data = [
        'card' => [
          'name' => @$payment_details['payment_wrap']['name'] ?: $address->getGivenName() . ' ' . $address->getFamilyName(),
          'number' => $payment_details['payment_wrap']['number'],
          'expiry_month' => $payment_details['payment_wrap']['expiration']['expiry_month'],
          'expiry_year' => $expiryYear,
          'cvd' => $payment_details['payment_wrap']['cvd'],
        ],
      ];
      $profiles_api->addCard(
        $customer_id,
        $card_data
      );
      $cards = $profiles_api->getCards($customer_id);

      return $cards;
    }
    catch (Exception $e) {
      if ($e->getCode() == 43) {
        // Get card number.
        $number = substr($payment_details['payment_wrap']['number'], -4);
        $card = $this->entityTypeManager->getStorage('commerce_payment_method')
          ->loadByProperties([
            'card_number' => $number,
            'uid' => $user->id(),
            'reusable' => TRUE,
          ]);
        if (!empty($card)) {
          $card = reset($card);
          $url = Link::fromTextAndUrl('click here', Url::fromRoute('entity.commerce_payment_method.collection', [
            'user' => $user->id(),
          ]))->toString();

          $this->messenger->addError($this->t("Unable to verify the credit card:
           operation failed. Please check the number on the card and ensure
           it's valid. If the card has expired, @url to delete the information.", [
             '@url' => $url,
           ]));
        }
        else {
          $this->messenger->addError('Unable to verify the credit card: ' . $e->getMessage());
        }

      }
      else {
        $this->messenger->addError('Unable to verify the credit card: ' . $e->getMessage());
      }

      throw new HardDeclineException('Unable to verify the credit card: ' . $e->getMessage());
    }
  }

  /**
   * Creates the payment method for a new customer.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *   The payment method.
   * @param array $payment_details
   *   The gateway-specific payment details.
   * @param \Beanstream\ProfilesApi $profiles_api
   *   The Beanstream Profiles API client.
   *
   * @return array
   *   The details of the last entered credit card.
   */
  protected function doCreatePaymentMethodForNewCustomer(
    PaymentMethodInterface $payment_method,
    array $payment_details,
    ProfilesApi $profiles_api
  ) {
    $owner = $payment_method->getOwner();

    $billing_profile = $payment_method->getBillingProfile();
    /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $address */
    $address = $billing_profile->get('address')->first();
    $phone = $billing_profile->get('phone');
    $card_data = ['validate' => TRUE];
    if (!empty($payment_details['payment_wrap']['cvd'])) {

      $expiryYear = \DateTime::createFromFormat('Y', $payment_details['payment_wrap']['expiration']['expiry_year']);
      $expiryYear = $expiryYear->format('y');
      $card_data = [
        'card' => [
          'name' => @$payment_details['payment_wrap']['name'] ?: $address->getGivenName() . ' ' . $address->getFamilyName(),
          'number' => $payment_details['payment_wrap']['number'],
          'expiry_month' => $payment_details['payment_wrap']['expiration']['expiry_month'],
          'expiry_year' => $expiryYear,
          'cvd' => $payment_details['payment_wrap']['cvd'],
        ],
      ];
    }

    try {
      // Create a new customer profile.
      $profile_create_token = [
        'billing' => [
          'name' => $address->getGivenName() . ' ' . $address->getFamilyName(),
          'email_address' => !empty($owner->getEmail()) ? $owner->getEmail() : $payment_details['bambora_customer_email'],
          'phone_number' => $phone->getString(),
          'address_line1' => $address->getAddressLine1(),
          'address_line2' => $address->getAddressLine2(),
          'city' => $address->getLocality(),
          'province' => $address->getAdministrativeArea(),
          'postal_code' => $address->getPostalCode(),
          'country' => $address->getCountryCode(),
        ],
      ];
      $profile_create_token['card'] = $card_data['card'];

      $customer_id = $profiles_api->createProfile($profile_create_token);

      $cards = $profiles_api->getCards($customer_id);
      // Save the remote customer ID.
      $owner = $payment_method->getOwner();
      if ($owner && $owner->isAuthenticated()) {
        $this->setRemoteCustomerId($owner, $customer_id);
        $owner->save();
      }

      return $cards;
    }
    catch (Exception $e) {
      $this->messenger->addError('Unable to verify the credit card: ' . $e->getMessage());
      throw new HardDeclineException('Unable to verify the credit card: ' . $e->getMessage());
    }
  }

  /**
   * Maps the Bambora credit card type to a Commerce credit card type.
   *
   * @param string $card_type
   *   The Bambora credit card type.
   *
   * @return string
   *   The Commerce credit card type.
   */
  protected function mapCreditCardType($card_type) {
    $map = [
      'AM' => 'amex',
      'DI' => 'dinersclub',
      'JB' => 'jcb',
      'MC' => 'mastercard',
      'NN' => 'discover',
      'VI' => 'visa',
    ];
    if (!isset($map[$card_type])) {
      $this->messenger->addError(sprintf('Unsupported credit card type "%s".',
        $card_type));
      throw new HardDeclineException(sprintf('Unsupported credit card type "%s".',
        $card_type));
    }

    return $map[$card_type];
  }

  /**
   * Map credit card and retrieve the type.
   */
  public function getCardType($cardNumber) {
    // Remove non-digits from the number.
    $cardNumber = preg_replace('/\D/', '', $cardNumber);

    switch ($cardNumber) {
      case(preg_match('/^4/', $cardNumber) >= 1):
        return 'VI';

      case(preg_match('/^5[1-5]/', $cardNumber) >= 1):
        return 'MC';

      case(preg_match('/^3[47]/', $cardNumber) >= 1):
        return 'AM';

      case(preg_match('/^3(?:0[0-5]|[68])/', $cardNumber) >= 1):
        return 'DI';

      case(preg_match('/^6(?:011|5)/', $cardNumber) >= 1):
        return 'NN';

      case(preg_match('/^(?:2131|1800|35\d{3})/', $cardNumber) >= 1):
        return 'JB';

      default:
        throw new HardDeclineException(sprintf("Unsupported credit card type."));
    }

  }

}
