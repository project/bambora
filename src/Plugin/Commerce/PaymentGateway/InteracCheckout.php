<?php

namespace Drupal\bambora\Plugin\Commerce\PaymentGateway;

use Beanstream\Exception;
use GuzzleHttp\ClientInterface;
use Drupal\commerce_price\Price;
use Drupal\bambora\ApiService;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_price\RounderInterface;
use Drupal\Component\Datetime\TimeInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_order\Entity\OrderInterface;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;

/**
 * Provides the Interac Off-site Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "interac_checkout",
 *   label = @Translation("Bambora (Interac)"),
 *   display_label = @Translation("Bambora Interac"),
 *
 *   forms = {
 *     "offsite-payment" = "Drupal\bambora\PluginForm\Bambora\InteracCheckoutForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "diners", "jcb", "mastercard", "discover", "visa",
 *   },
 * )
 */
class InteracCheckout extends OffsitePaymentGatewayBase {

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The price rounder.
   *
   * @var \Drupal\commerce_price\RounderInterface
   */
  protected $rounder;

  /**
   * The time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The Bambora service class.
   *
   * @var \Drupal\bambora\ApiService
   */
  protected $bamboraService;

  /**
   * Constructs a new InteracCheckout object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory.
   * @param \GuzzleHttp\ClientInterface $client
   *   The client.
   * @param \Drupal\commerce_price\RounderInterface $rounder
   *   The price rounder.
   * @param \Drupal\bambora\ApiService $bambora_service
   *   The commerce bambora service class.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager $payment_type_manager,
    PaymentMethodTypeManager $payment_method_type_manager,
    TimeInterface $time,
    LoggerChannelFactoryInterface $logger_channel_factory,
    ClientInterface $client,
    RounderInterface $rounder,
    ApiService $bambora_service
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_type_manager,
      $payment_type_manager,
      $payment_method_type_manager,
      $time
    );

    $this->logger = $logger_channel_factory->get('bambora_interac');
    $this->httpClient = $client;
    $this->rounder = $rounder;
    $this->bamboraService = $bambora_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('logger.factory'),
      $container->get('http_client'),
      $container->get('commerce_price.rounder'),
      $container->get('bambora.api_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'merchant_id' => '',
      'payments_api_key' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#default_value' => $this->configuration['merchant_id'],
      '#required' => TRUE,
    ];

    $form['payments_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Payments API Key'),
      '#description' => $this->t('This is the API Passcode found under
        Administration -> Account -> Order Settings.'),
      '#default_value' => $this->configuration['payments_api_key'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['merchant_id'] = $values['merchant_id'];
      $this->configuration['payments_api_key'] = $values['payments_api_key'];
    }
  }

  /**
   * Return payment api.
   */
  public function getBamboraService() {
    // Process first request to get merchant data.
    return $this->bamboraService;
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);

    // Void the payment.
    try {
      $beanstream = $this->bamboraService->payments($payment->getPaymentGateway(), 'api');

      $remote_id = $payment->getRemoteId();

      $beanstream
        ->payments()
        ->voidPayment(
          $remote_id,
          $this->rounder->round($payment->getAmount())->getNumber()
        );
    }
    catch (Exception $e) {
      throw new PaymentGatewayException('Could not void the payment. Message: ' . $e->getMessage());
    }

    $payment->setState('authorization_voided');
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $amount = $this->rounder->round($amount);
    $this->assertRefundAmount($payment, $amount);

    // Refund the payment.
    try {
      $beanstream = $this->bamboraService->payments($payment->getPaymentGateway(), 'api');
      $remote_id = $payment->getRemoteId();

      $beanstream
        ->payments()
        ->returnPayment(
          $remote_id,
          $amount->getNumber(),
          $payment->getOrderId()
        );
    }
    catch (Exception $e) {
      throw new InvalidRequestException('Could not refund the payment. Message: ' . $e->getMessage());
    }

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRequest(PaymentInterface $payment) {
    $order = $payment->getOrder();

    $orderComments = '';
    foreach ($order->getItems() as $product) {
      $orderComments .= ($orderComments ? ", " : "") . ucfirst($product->getTitle()) . (($product->getQuantity() > 1) ? ("(" . $product->getQuantity() . "x)") : "");
    }
    /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $address */
    $address = $order->getBillingProfile()->address->first();
    $phone = $order->getBillingProfile()->phone;
    // Authorize the payment.
    $data = [
      'order_number' => $payment->getOrderId(),
      'amount' => $this->rounder->round($payment->getAmount())->getNumber(),
      'payment_method' => 'interac',
      'billing' => [
        'name' => $address->getGivenName() . ' ' . $address->getFamilyName(),
        'email_address' => $order->getEmail(),
        'phone_number' => $phone->getString(),
        'address_line1' => $address->getAddressLine1(),
        'city' => $address->getLocality(),
        'province' => $address->getAdministrativeArea(),
        'postal_code' => $address->getPostalCode(),
        'country' => $address->getCountryCode(),
      ],
      'comments' => $orderComments,
    ];

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    $this->messenger()->addMessage($this->t('You have canceled checkout at @gateway but may resume the checkout process here when you are ready.', [
      '@gateway' => $this->getDisplayLabel(),
    ]));

    // Set order state back to review.
    $order->set('checkout_step', 'review');
    $order->save();
  }

}
