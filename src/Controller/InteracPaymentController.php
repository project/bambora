<?php

namespace Drupal\bambora\Controller;

use Drupal\Core\Url;
use Beanstream\Exception;
use Drupal\bambora\ApiService;
use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\Access\AccessException;
use Drupal\Core\Controller\ControllerBase;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_order\Event\OrderEvents;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\commerce\Response\NeedsRedirectException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;

/**
 * This is a interac controller for mocking an off-site gateway.
 */
class InteracPaymentController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * The Bambora service class.
   *
   * @var \Drupal\bambora\ApiService
   */
  protected $bamboraService;

  /**
   * Constructs a new InteracPaymentController object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\bambora\ApiService $bambora_service
   *   The commerce bambora service class.
   */
  public function __construct(RequestStack $request_stack, ApiService $bambora_service) {
    $this->currentRequest = $request_stack->getCurrentRequest();
    $this->bamboraService = $bambora_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('bambora.api_service')
    );
  }

  /**
   * Callback method which accepts POST.
   *
   * @throws \Drupal\commerce\Response\NeedsRedirectException
   */
  public function post() {
    $orderNumber = $this->currentRequest->query->get('order_number');
    if (empty($orderNumber)) {
      $this->messenger()->addError($this->t('Please ensure you have items in your provided order.'));
      $url = Url::fromRoute('commerce_cart.page')->toString(TRUE);
      return new TrustedRedirectResponse($url->getGeneratedUrl());
    }
    // Load order and get payment bambora api object.
    $order = $this->entityTypeManager()->getStorage('commerce_order')
      ->load($orderNumber);
    // Load payments api.
    $paymentGateway = $this->entityTypeManager()->getStorage('commerce_payment_gateway')
      ->load('interac_payment');
    $paymentApi = $this->bamboraService->payments($paymentGateway, 'api');
    $result = FALSE;

    // Try initial POST.
    try {
      $orderData = $order->getData('interac_data');
      $referrer = Url::fromRoute('<current>', [], ['absolute' => 'true'])->toString(TRUE);
      $result = $paymentApi->makePayment($orderData, $referrer->getGeneratedUrl());
    }
    catch (Exception $e) {
      $this->getLogger('bambora_interac')
        ->warning('[Bambora Interac API] Payment request failed. Order id is @order_id. Message from API: @msg', [
          '@order_id' => $order->id(),
          '@msg' => $e->getMessage(),
        ]);
    }
    // If no result, go back to payment step.
    if (empty($result)) {
      $this->getLogger('bambora_interac')
        ->warning('[Bambora Interac API] No payment token available. Order id is @order_id.', [
          '@order_id' => $order->id(),
        ]);

      $this->messenger()
        ->addWarning($this->t('We are experiencing issues with the Payment Gateway. Please try again.'));
      $params = [
        'commerce_order' => $order->id(),
        'step' => 'review',
      ];
      $backUrl = Url::fromRoute('commerce_checkout.form', $params)->toString(TRUE);
      return new TrustedRedirectResponse($backUrl->getGeneratedUrl());
    }

    // Save merchant id to the order.
    $order->setData('merchant_data', $result['merchant_data']);
    // A placed order should never be locked.
    $order->unlock();
    $order->save();

    // Redirect to Interac Online.
    $decodeHtml = urldecode($result['contents']);

    $headers = [
      'Referrer-policy' => 'no-referrer-when-downgrade',
    ];
    return new HtmlResponse($decodeHtml, 200, $headers);
  }

  /**
   * Catch funded response.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   */
  public function catchFunded(Request $request) {
    $response = $request->request->all();
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $this->entityTypeManager()->getStorage('commerce_order')
      ->load($response['IDEBIT_INVOICE']);
    $result = FALSE;

    // Continue payment.
    $paymentGateway = $this->entityTypeManager()->getStorage('commerce_payment_gateway')
      ->load('interac_payment');
    $paymentApi = $this->bamboraService->payments($paymentGateway, 'api');
    try {
      $merchant = $order->getData('merchant_data');
      $finalRequestData = [
        'payment_method' => 'interac',
        'interac_response' => [
          'IDEBIT_MERCHDATA' => $response['IDEBIT_MERCHDATA'],
          'IDEBIT_INVOICE' => $response['IDEBIT_INVOICE'],
          'IDEBIT_AMOUNT' => $response['IDEBIT_AMOUNT'],
          'IDEBIT_FUNDEDURL' => $response['IDEBIT_FUNDEDURL'],
          'IDEBIT_NOTFUNDEDURL' => $response['IDEBIT_NOTFUNDEDURL'],
          'IDEBIT_ISSLANG' => $response['IDEBIT_ISSLANG'],
          'IDEBIT_TRACK2' => $response['IDEBIT_TRACK2'],
          'IDEBIT_ISSCONF' => $response['IDEBIT_ISSCONF'],
          'IDEBIT_ISSNAME' => $response['IDEBIT_ISSNAME'],
          'IDEBIT_VERSION' => $response['IDEBIT_VERSION'],
        ],
      ];
      $result = $paymentApi->continuePayment($finalRequestData, $merchant);
    }
    catch (Exception $e) {
      $this->getLogger('bambora_interac')
        ->warning('[Bambora Interac API] Payment request failed. Order id is @order_id. Message from API: @msg', [
          '@order_id' => $order->id(),
          '@msg' => $e->getMessage(),
        ]);
    }

    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $order->get('payment_gateway')->entity;
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    if (!$payment_gateway_plugin instanceof OffsitePaymentGatewayInterface) {
      // A placed order should never be locked.
      $order->unlock();
      $order->save();
      throw new AccessException('The payment gateway for the order does not implement ' . OffsitePaymentGatewayInterface::class);
    }
    if (empty($result)) {
      $this->messenger()
        ->addWarning($this->t('We are experiencing issues with the Payment Gateway. Please try again.'));
      $params = [
        'commerce_order' => $order->id(),
        'step' => 'review',
      ];
      // A placed order should never be locked.
      $order->unlock();
      $order->save();
      $backUrl = Url::fromRoute('commerce_checkout.form', $params)->toString(TRUE);
      return new TrustedRedirectResponse($backUrl->getGeneratedUrl());
    }
    try {
      $transactionId = $this->onVerifyPayment($order, $result);

      $session = \Drupal::service('session');
      if (!$session->isStarted()) {
        $session->migrate();
      }
      // Call order paid event.
      $event = new OrderEvent($order);
      $this->dispatcher->dispatch($event, OrderEvents::ORDER_PAID);
      $order->setData('paid_event_dispatched', TRUE);
      $order->save();
      $redirect_step_id = 'complete';
    }
    catch (PaymentGatewayException $e) {
      // Log errors.
      $this->getLogger('bambora_interac')->error($e->getMessage());
      $this->messenger()
        ->addWarning($e->getMessage());
      // The Payment Gateway plugin should take care of notifying user.
      $redirect_step_id = 'review';
      // A placed order should never be locked.
      $order->unlock();
      $order->save();
    }
    throw new NeedsRedirectException(Url::fromRoute('commerce_checkout.form', [
      'commerce_order' => $order->id(),
      'step' => $redirect_step_id,
    ])->toString());

  }

  /**
   * Processes the Bambora API funded return request.
   *
   * This method checks the response payload and throws
   * PaymentGatewayException to return user back to review -step or
   * processed payment data if payload is valid and payment was made.
   *
   * @param object $order
   *   The order.
   * @param mixed $result
   *   The result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onVerifyPayment($order, $result) {
    // Log fail order.
    if (empty($result['approved'])) {
      $this->messenger()
        ->addWarning($this->t('Payment was cancelled.'));
      throw new PaymentGatewayException('Payment was cancelled.');
    }
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $transactionId = !empty($result['id']) ? $result['id'] : $order->id();
    $payment = $payment_storage->create(
      [
        // Total price number was sent with the request (and is part of
        // the data used to get the token), so we can trust the
        // order total price to be still the same.
        'amount' => $order->getTotalPrice(),
        'payment_gateway' => 'interac_payment',
        'order_id' => $order->id(),
        'remote_state' => 'payment completed successfully',
      ]);
    // Clear data.
    $order->setData('cc_data', NULL);
    $payment->setState('completed');
    $payment->setRemoteId($transactionId);
    try {
      $payment->save();
    }
    catch (PaymentGatewayException $e) {
      $this->getLogger('bambora_interac')->error($e->getMessage());
    }
    return $transactionId;
  }

  /**
   * Catch unfunded response.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   */
  public function catchUnfunded(Request $request) {
    $response = $request->query->get('IDEBIT_MERCHDATA');

    // If response amount if empty, fall back.
    if (!empty($response)) {
      $order_id = NULL;
      $database = Database::getConnection();
      $result = $database->query("SELECT order_id FROM {commerce_order} WHERE CONVERT(data USING latin1) LIKE :code", [
        ':code' => '%' . $response . '%',
      ]);
      $data = $result->fetchAssoc();
      if (!empty($data['order_id'])) {
        $order_id = $data['order_id'];
      }
    }
    // Log errors.
    $this->getLogger('bambora_interac')
      ->warning('[Bambora Interac API] Unfunded response received form bambora. Order id is @order_id.', [
        '@order_id' => $order_id,
      ]);

    $this->messenger()
      ->addWarning($this->t('We are experiencing issues with the Payment Gateway. Please try again.'));
    $params = [
      'commerce_order' => $order_id,
      'step' => 'review',
    ];
    // A placed order should never be locked.
    $order = $this->entityTypeManager()->getStorage('commerce_order')
      ->load($order_id);

    if (is_object($order)) {
      $order->unlock();
      // Set order state back to review.
      $order->set('checkout_step', 'review');
      $order->save();
    }
    if (!empty($order_id)) {
      $backUrl = Url::fromRoute('commerce_checkout.form', $params)->toString(TRUE);
      return new TrustedRedirectResponse($backUrl->getGeneratedUrl());
    }

    $backUrl = Url::fromRoute('commerce_cart.page')->toString(TRUE);
    return new TrustedRedirectResponse($backUrl->getGeneratedUrl());
  }

}
