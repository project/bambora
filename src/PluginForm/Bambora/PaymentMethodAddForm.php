<?php

namespace Drupal\bambora\PluginForm\Bambora;

use Drupal\commerce_payment\CreditCard;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm as BasePaymentMethodAddForm;

/**
 * Defines PaymentMethodAddForm class.
 */
class PaymentMethodAddForm extends BasePaymentMethodAddForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;

    $form['payment_details'] = [
      '#parents' => array_merge($form['#parents'], ['payment_details']),
      '#type' => 'container',
      '#payment_method_type' => $payment_method->bundle(),
    ];
    if ($payment_method->bundle() == 'credit_card') {
      $form['payment_details'] = $this->buildCreditCardForm($form['payment_details'], $form_state);
    }
    elseif ($payment_method->bundle() == 'paypal') {
      $form['payment_details'] = $this->buildPayPalForm($form['payment_details'], $form_state);
    }
    // Move the billing information below the payment details.
    if (isset($form['billing_information'])) {
      $form['billing_information']['#weight'] = 10;
    }

    $form['reusable'] = [
      '#type' => 'value',
      '#value' => $payment_method->isReusable(),
    ];
    if (!empty($form['#allow_reusable'])) {
      if (!empty($form['#always_reusable'])) {
        $form['reusable']['#value'] = TRUE;
      }
      else {
        $form['reusable'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Save this payment method for later use'),
          '#default_value' => FALSE,
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildCreditCardForm(array $element, FormStateInterface $form_state) {
    $defaultForm = parent::buildCreditCardForm($element, $form_state);
    // Placeholder for the detected card type. Set by validateCreditCardForm().
    $element['type'] = [
      '#type' => 'hidden',
      '#value' => '',
    ];
    $element['payment_wrap'] = [
      '#type' => 'container',
    ];
    $element['payment_wrap']['info'] = [
      '#markup' => $this->t('<div class="results-displayed">
  <span class="results-displayed__icon">
    <svg xmlns="http://www.w3.org/2000/svg" width="13.667" height="13.667" viewBox="0 0 13.667 13.667"><defs><style>.a{fill:#7f7f7f;}</style></defs><path class="a" d="M6.833,0a6.833,6.833,0,1,0,6.833,6.834A6.841,6.841,0,0,0,6.833,0Zm0,12.425a5.591,5.591,0,1,1,5.591-5.591A5.6,5.6,0,0,1,6.833,12.425Z"/><path class="a" d="M145.83,70a.828.828,0,1,0,.828.829A.829.829,0,0,0,145.83,70Z" transform="translate(-138.996 -67.101)"/><path class="a" d="M150.621,140a.621.621,0,0,0-.621.621v3.727a.621.621,0,0,0,1.242,0v-3.727A.621.621,0,0,0,150.621,140Z" transform="translate(-143.787 -134.202)"/></svg>
  </span>
  <span class="results-displayed__text">Your billing information must match the billing address for the credit card entered below or we will be unable to process your payment.</span>
</div>'),
      '#weight' => -1,
    ];

    $element['payment_wrap']['number'] = [
      '#type' => 'textfield',
      '#prefix' => '<div class="row"><div class="col-12 col-md-6">',
      '#suffix' => '</div>',
      '#title' => $this->t('Card number'),
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => $this->t('Card Number'),
        'autocomplete' => 'cc-csc',
      ],
      '#maxlength' => 19,
      '#size' => 20,
    ];

    $element['payment_wrap']['name'] = [
      '#type' => 'textfield',
      '#prefix' => '<div class="col-12 col-md-6">',
      '#suffix' => '</div></div>',
      '#title' => $this->t('Card Owner'),
      '#attributes' => [
        'placeholder' => $this->t('Card Owner'),
        'autocomplete' => 'off',
      ],
    ];

    $element['payment_wrap']['expiration'] = [
      '#type' => 'container',
      '#prefix' => '<div class="row"><div class="col-12 col-md-6">',
      '#suffix' => '</div>',
      '#title' => $this->t('Expiration date'),
      '#attributes' => [
        'class' => ['credit-card-form__expiration'],
        'autocomplete' => 'off',
      ],
    ];

    $element['payment_wrap']['expiration']['expiry_month'] = $defaultForm['expiration']['month'];
    $element['payment_wrap']['expiration']['expiry_month']['#attributes']['autocomplete'] = 'cc-csc';
    $element['payment_wrap']['expiration']['divider'] = $defaultForm['expiration']['divider'];
    $element['payment_wrap']['expiration']['expiry_year'] = $defaultForm['expiration']['year'];
    $element['payment_wrap']['expiration']['expiry_year']['#attributes']['autocomplete'] = 'cc-csc';

    $element['payment_wrap']['cvd'] = [
      '#type' => 'textfield',
      '#prefix' => '<div class="col-12 col-md-6">',
      '#suffix' => '</div></div>',
      '#title' => $this->t('CVV'),
      '#required' => TRUE,
      '#validated' => TRUE,
      '#attributes' => [
        'placeholder' => $this->t('CVV'),
        'autocomplete' => 'off',
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function validateCreditCardForm(array &$element, FormStateInterface $form_state) {
    $values = $form_state->getValue($element['#parents']);
    // The JS library performs its own validation.
    if (!CreditCard::validateExpirationDate($values['payment_wrap']['expiration']['expiry_month'], $values['payment_wrap']['expiration']['expiry_year'])) {
      $form_state->setError($element['payment_wrap']['expiration']['expiry_year'], $this->t('You have entered an expired credit card.'));
    }

    $card_type = CreditCard::detectType($values['payment_wrap']['number']);
    if (!$card_type) {
      $form_state->setError($element['payment_wrap']['number'], $this->t('You have entered a credit card number of an unsupported card type.'));
      return;
    }
    if (!CreditCard::validateNumber($values['payment_wrap']['number'], $card_type)) {
      $form_state->setError($element['payment_wrap']['number'], $this->t('You have entered an invalid credit card number.'));
    }
    if (!CreditCard::validateSecurityCode($values['payment_wrap']['cvd'], $card_type)) {
      $form_state->setError($element['payment_wrap']['cvd'], $this->t('You have entered an invalid CVV.'));
    }

    // Persist the detected card type.
    $form_state->setValueForElement($element['type'], $card_type->getId());
  }

  /**
   * {@inheritdoc}
   */
  public function submitCreditCardForm(array $element, FormStateInterface $form_state) {
    $values = $form_state->getValue($element['#parents']);
    $card_type = CreditCard::detectType($values['payment_wrap']['number']);
    $this->entity->card_type = $card_type;
    $this->entity->card_number = substr($values['payment_wrap']['number'], -4);
    $this->entity->card_exp_month = $values['payment_wrap']['expiration']['expiry_month'];
    $this->entity->card_exp_year = $values['payment_wrap']['expiration']['expiry_year'];
  }

}
