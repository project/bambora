<?php

namespace Drupal\bambora\PluginForm\Bambora;

use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;

/**
 * Define InteracCheckoutForm class.
 */
class InteracCheckoutForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\bambora\Plugin\Commerce\PaymentGateway\InteracCheckoutInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();

    $data = $payment_gateway_plugin->buildRequest($payment);
    // Set additional data for order.
    $order = $payment->getOrder();
    $order->setData('interac_data', $data);
    $order->save();

    // Redirect to our custom controller.
    $query = [
      'order_number' => $order->id(),
    ];
    $redirect_url = Url::fromRoute('bambora.interac_redirect');
    $redirect_url->setOption('query', $query);
    $redirect_url = $redirect_url->toString();
    throw new NeedsRedirectException($redirect_url);
  }

}
