# Bambora Commerce Drupal Module

## Overview

The Bambora Commerce Drupal Module provides seamless integration of Bambora's payment gateway with Drupal websites. This module allows Drupal site owners to securely accept online payments, manage transactions, and enhance the overall shopping experience for their customers.

## Features

- **Secure Payment Processing:** Utilize Bambora's robust and secure payment gateway to process online transactions with confidence.

- **Easy Integration:** Effortlessly integrate Bambora Commerce with your Drupal website, ensuring a smooth and hassle-free payment experience for your customers.

- **Multiple Payment Methods:** Support a variety of payment methods, including credit cards and alternative payment options, to cater to a diverse customer base.

- **Transaction Management:** View and manage transactions directly within the Drupal admin interface, providing real-time insights into payment activities.

- **Customizable Checkout:** Customize the checkout process to align with your branding and improve the overall user experience.

- **PCI DSS Compliance:** Ensure compliance with PCI DSS standards to protect sensitive customer payment information.

## Installation

Follow these steps to install the Bambora Commerce Drupal Module:

1. Download the module from the [Drupal Module Repository](https://www.drupal.org/project/bambora).

2. Extract the module files to the `modules` directory of your Drupal installation.

3. Enable the Bambora Commerce module in the Drupal admin interface.

4. Configure the module with your Bambora account credentials and other required settings.

5. Test the integration by processing a test transaction in a sandbox environment.

## Configuration

To configure the Bambora Commerce Drupal Module, navigate to the Drupal admin interface:

1. Go to `Configuration` > `Payment and Payment Gateways` > `Bambora Commerce`.

2. Enter your Bambora account credentials, including Merchant ID and Secret Key.

3. Adjust additional settings as needed, such as currency, payment methods, and transaction modes.

4. Save the configuration.

## Usage

Once the module is installed and configured, the Bambora Commerce payment gateway will be available as a payment option during the checkout process. Customers can securely enter their payment details, and the module will handle the transaction processing.

## Support

For support and assistance, please visit the [Bambora Support Center](https://www.bambora.com/support/).

## Contribution

We welcome contributions to enhance the functionality and compatibility of the Bambora Commerce Drupal Module. Feel free to submit issues, feature requests, or pull requests on the [GitHub repository](https://git.drupalcode.org/project/bambora).

## License

This module is licensed under the [GNU General Public License (GPL)](https://www.gnu.org/licenses/gpl-2.0.html). See the LICENSE file for more details.
